package com.example.calcolatrice2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var one:Button
    lateinit var two:Button
    lateinit var three:Button
    lateinit var four:Button
    lateinit var five:Button
    lateinit var six:Button
    lateinit var seven:Button
    lateinit var eight:Button
    lateinit var nine:Button
    lateinit var zero:Button
    lateinit var plus:Button
    lateinit var minus:Button
    lateinit var divide:Button
    lateinit var per:Button
    lateinit var cancel:Button
    lateinit var point:Button
    lateinit var equals:Button
    lateinit var espressione:TextView
    lateinit var exp:String
    lateinit var membro: String
    lateinit var m:String
    var previous=-1
    var last=-1
    lateinit var lexp:MutableList<String>



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        one=findViewById(R.id.one)
        two=findViewById(R.id.two)
        three=findViewById(R.id.three)
        four=findViewById(R.id.four)
        five=findViewById(R.id.five)
        six=findViewById(R.id.six)
        seven=findViewById(R.id.seven)
        eight=findViewById(R.id.eight)
        nine=findViewById(R.id.nine)
        zero=findViewById(R.id.zero)
        point=findViewById(R.id.point)
        plus=findViewById(R.id.plus)
        divide=findViewById(R.id.divide)
        minus=findViewById(R.id.minus)
        per=findViewById(R.id.per)
        cancel=findViewById(R.id.cancel)
        equals=findViewById(R.id.equals)
        espressione=findViewById(R.id.espressione)
        membro=getString(R.string.empty)
        var l:MutableList<String>
        lexp=mutableListOf()

        one.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"1"
            m=membro
            membro=m+"1"
            last=1
        }
        two.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"2"
            m=membro
            membro=m+"2"
            last=1
        }
        three.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"3"
            m=membro
            membro=m+"3"
            last=1
        }
        four.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"4"
            m=membro
            membro=m+"4"
            last=1
        }
        five.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"5"
            m=membro
            membro=m+"5"
            last=1
        }
        six.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"6"
            m=membro
            membro=m+"6"
            last=1
        }
        seven.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"7"
            m=membro
            membro=m+"7"
            last=1
        }
        eight.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"8"
            m=membro
            membro=m+"8"
            last=1
        }
        nine.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"9"
            m=membro
            membro=m+"9"
            last=1
        }
        zero.setOnClickListener {
            exp=espressione.text.toString()
            espressione.text=exp +"0"
            m=membro
            membro=m+"0"
            last=0
        }
        plus.setOnClickListener {
            if(last!=2&&last!=3&&last!=5){
                exp=espressione.text.toString()
                espressione.text=exp +"+"
                lexp.add(membro)
                lexp.add("+")
                membro =getString(R.string.empty)
                last=2
                previous=1
            }

        }
        minus.setOnClickListener {
            if(last!=2&&last!=3&&last!=5) {
                exp = espressione.text.toString()
                espressione.text = exp + "-"
                lexp.add(membro)
                lexp.add("-")
                membro =getString(R.string.empty)
                last=2
                previous=1
            }

        }
        per.setOnClickListener {
            if(last!=2&&last!=3&&last!=5) {
                exp = espressione.text.toString()
                espressione.text = exp + "X"
                lexp.add(membro)
                lexp.add("X")
                membro =getString(R.string.empty)
                last = 3
                previous = 1
            }

        }
        divide.setOnClickListener {
            if(last!=2&&last!=3&&last!=5) {
                exp = espressione.text.toString()
                espressione.text = exp + "/"
                lexp.add(membro)
                lexp.add("/")
                membro =getString(R.string.empty)
                last=3
                previous=1
            }

        }
        point.setOnClickListener {
            if(last!=2&&last!=3&&last!=5&&previous!=5) {
                exp = espressione.text.toString()
                espressione.text = exp + "."
                lexp.add(membro)
                lexp.add(".")
                membro =getString(R.string.empty)
                last = 5
                previous = 5
            }

        }
        cancel.setOnClickListener {
            espressione.text=getString(R.string.empty)
            lexp= mutableListOf()
            previous=-1
            last=-1
            membro=getString(R.string.empty)


        }
        equals.setOnClickListener {
            lexp.add(membro)
            membro=getString(R.string.empty)
            l=mutableListOf()
            for(s in lexp){
                if(!s.isNullOrEmpty()){
                    l.add(s)
                }
            }
            l=depuntazione(l)
            previous=5
            last=1
            lexp.clear()
            risultato(l,1)
        }
    }

    private fun log10(id:Int): Int{

        if(id<10){
            return 1
        }
        else{
            return 1+log10(id/10)
        }

    }
    private fun risultato(l:MutableList<String>,opp:Int){
        var val1:Float
        var val2:Float
        var val3:Float
        val3=0F
        var t:String
        var t1:String
        t1=""
        var t2:String
        t2=""
        var tt:String
        var op=0
        var point=0
        val1=0F
        var ops=0
        var vals=0
        var l1:MutableList<String>
        l1= mutableListOf()

        if(l.size==1){
            espressione.text=l.last()
            t=l.last()
            for(c in t){
                if(point==0&&!c.toString().equals(".")){
                    tt=t1
                    t1=tt+c.toString()
                }else{
                    if(c.toString().equals(".")){
                        lexp.add(t1)
                        point=1
                    }
                    else{
                        tt=t2
                        t2=tt+c.toString()
                    }
                }
            }
            if(t2.toInt()!=0){
                lexp.add(".")
                lexp.add(t2)
                espressione.text=l.last()
            }
            else{
                espressione.text=t1.toInt().toString()
            }

        }
        else{
            for(s in l){
                //se primo numero
                if(!s.equals("X")&&!s.equals("+")&&!s.equals("-")&&!s.equals("/")&&vals==0){
                    vals=1
                    val1=s.toFloat()
                }
                else{
                    //se altro numero
                    if (!s.equals("X")&&!s.equals("+")&&!s.equals("-")&&!s.equals("/")&&vals==1){
                        val2=s.toFloat()
                        ops=1
                        //se operazione giusta
                        if(op==opp){
                            when(op){
                                1-> {
                                    val3 = val1 * val2
                                }
                                2->{
                                    val3 = val1 / val2
                                }
                                3->{
                                    val3 = val1 - val2
                                }
                                4->{
                                    val3 = val1 + val2
                                }
                            }
                            val1=val3

                        }
                        //se operazione sbagliata
                        else{
                            l1.add(val1.toString())
                            when(op){
                                1->l1.add("X")
                                2->l1.add("/")
                                4->l1.add("+")
                                3->l1.add("-")
                            }
                            val1=val2
                            ops=0
                        }
                    }
                    //se operatore
                    else{
                        when(s){
                            "X"->op=1
                            "/"->op=2
                            "+"->op=4
                            "-"->op=3
                        }

                    }
                }
            }
            if(ops==1){
                l1.add(val1.toString())
            }
            else{
                l1.add(val1.toString())
            }
            when(opp){
                1->risultato(l1,2)
                2->risultato(l1,3)
                3->risultato(l1,4)
                4->risultato(l1,0)
            }


        }
    }
    private fun exponent(b: Int,e:Int):Int{
        if(e==0){
            return 1
        }
        if(e==1){
            return b
        }
        return b*exponent(b,e-1)
    }
    private fun depuntazione(l:MutableList<String>):MutableList<String>{

        var base:Float
        var post:String
        var punto=0
        base=0F
        var ln:MutableList<String>
        ln= mutableListOf()
        for(s in l){
            println(s)
            if(!s.equals("X")&&!s.equals("+")&&!s.equals("-")&&!s.equals("/")&&!s.equals(".")&&punto==0){
                base=s.toInt().toFloat()
            }
            else{
                if(s.equals(".")){
                    punto =1
                }
                else{
                    if(!s.equals("X")&&!s.equals("+")&&!s.equals("-")&&!s.equals("/")&&!s.equals(".")&&punto==1){
                        post="0."+s
                        base=base+post.toFloat()
                        punto=0

                    }
                    else{
                        if(s.equals("X")||s.equals("+")||s.equals("/")||s.equals("-")){
                            ln.add(base.toString())
                            ln.add(s)
                        }
                    }
                }
            }
        }
        ln.add(base.toString())
        return ln
    }
}